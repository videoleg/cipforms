﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CipForms.Data;
using ElementModel = CipForms.Data.Models.Element;

namespace CipForms.Logic.Operations.Element
{
    public class Index
    {

        private readonly DatabaseContext _databaseContext;

        public Index(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public async Task<List<ElementModel>> InvokeAsync()
        {
            return await _databaseContext.elements.AsQueryable().ToListAsync();
        }
    }
}
