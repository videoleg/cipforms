﻿using System;
using System.Threading.Tasks;
using CipForms.Logic.Models;
using Contract = CipForms.Logic.Contracts.Element.Update;
using Model = CipForms.Data.Models.Element;
using CipForms.Data;

namespace CipForms.Logic.Operations.Element
{
    public class Update : AppOperation<Contract, Model>
    {
        public Update(DatabaseContext databaseContext) : base(databaseContext) { }

        public override async Task<AppContext<Contract, Model>> InvokeAsync(AppContext<Contract, Model> context)
        {
            // assign model properties to the contract
            context.Parameters.Assign(context.Model);
    
            if (!context.Parameters.IsValid())
            {
                context.Errors = context.Parameters.Errors;
                return context;
            }

            // assign contract properties to the model
            context.Parameters.Save(context.Model);

            _databaseContext.elements.Update(context.Model);
            await _databaseContext.SaveChangesAsync();


            return context;
        }
    }
}
