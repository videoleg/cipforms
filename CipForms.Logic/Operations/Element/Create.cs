﻿using System;
using CipForms.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using CipForms.Logic.Interfaces;
using CipForms.Logic.Models;
using CipForms.Logic.Contracts;
using Contract = CipForms.Logic.Contracts.Element.Create;
using Model = CipForms.Data.Models.Element;

namespace CipForms.Logic.Operations.Element
{
    public class Create : AppOperation<Contract, Model>
    {
        public Create(DatabaseContext databaseContext) : base(databaseContext) {}

        public override async Task<AppContext<Contract, Model>> InvokeAsync(AppContext<Contract, Model> context)
        {
            Model element = context.Parameters.Save();

            if (!context.Parameters.IsValid())
            {
                context.Errors = context.Parameters.Errors;
                return context;
            }

            _databaseContext.elements.Add(element);
            await _databaseContext.SaveChangesAsync();

            context.Model = element;

            return context;
        }

      
    }
}