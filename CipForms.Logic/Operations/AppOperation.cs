﻿using System;
using CipForms.Logic.Interfaces;
using CipForms.Logic.Contracts;
using CipForms.Logic.Models;
using System.Threading.Tasks;
using CipForms.Data;

namespace CipForms.Logic.Operations
{
    public abstract class AppOperation<TParams, TModel> : IOperation<TParams, TModel> where TParams : AppContract<TModel> where TModel : new()
    {

        protected readonly DatabaseContext _databaseContext;

        public AppOperation(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public abstract Task<AppContext<TParams, TModel>> InvokeAsync(AppContext<TParams, TModel> context);




        //Task<IContext<TParams, TModel>> IOperation<TParams, TModel>.InvokeAsync(IContext<TParams, TModel> context)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
