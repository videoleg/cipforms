﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using MapperConfiguration = AutoMapper.MapperConfiguration;
using Mapper = AutoMapper.Mapper;

namespace CipForms.Logic.Contracts
{

    public class AppContract<TModel>: IValidatableObject where TModel : new()
    {
        public IEnumerable<ValidationResult> Errors { get; set; } = new List<ValidationResult>();

        public IEnumerable<ValidationResult> Validate(ValidationContext ctx)
        {
            var errors = new List<ValidationResult>();
            return errors;
        }

        public bool IsValid()
        {
            var errors = new List<ValidationResult>();
            var context = new ValidationContext(this);
            if (!Validator.TryValidateObject(this, context, errors, true)) {
                Errors = errors;
                return false;
            }
            return true;
        }


        /// <summary>
        ///  Assign model properties to the contract
        /// </summary>
        /// <param name="model"></param>
        public void Assign(TModel model)
        {
            Type sourceType = typeof(TModel), destType = this.GetType();
            var mapping = new MapperConfiguration(cfg => {
                // TODO
                cfg.CreateMap(sourceType, destType);
                // assing only attributes presented in json
         
            });
            var mapper = new Mapper(mapping);
            mapper.Map(model, this, sourceType, destType);
        }

        /// <summary>
        ///  Assigns contract properties to the model
        /// </summary>
        /// <param name="model"></param>
        ///
        public TModel Save() => Save(new TModel());


        public TModel Save(TModel model)
        {
            Map(this, model, this.GetType(), typeof(TModel));
            return model;
        }

        private void Map(object source, object dest, Type sourceType, Type destType)
        {
            var mapping = new MapperConfiguration(cfg => cfg.CreateMap(sourceType, destType));
            var mapper = new Mapper(mapping);
            mapper.Map(source, dest, sourceType, destType);
        }

        public List<string> Specified = new();
    }
}
