﻿using System;
using System.ComponentModel.DataAnnotations;
using Model = CipForms.Data.Models.Element;

namespace CipForms.Logic.Contracts.Element
{
    public class Create : AppContract<Model>
    {
        [Required]
        [MinLength(2)]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Editable(false, AllowInitialValue = true)]
        public DateTime CreatedAt { get; } = DateTime.Now;

        [Editable(false, AllowInitialValue = true)]
        public DateTime UpdatedAt { get; } = DateTime.Now;
    }
}
