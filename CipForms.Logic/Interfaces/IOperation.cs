﻿using System;
using System.Threading.Tasks;
using CipForms.Logic.Interfaces;
using CipForms.Logic.Contracts;
using CipForms.Logic.Models;

namespace CipForms.Logic.Interfaces
{
    public interface IOperation<TParams, TModel> where TParams : AppContract<TModel> where TModel : new()
    {
        public Task<AppContext<TParams, TModel>> InvokeAsync(AppContext<TParams, TModel> context);
    }
}
