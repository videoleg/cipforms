﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CipForms.Logic.Contracts;

namespace CipForms.Logic.Models
{
    public class AppContext<TParams, TModel> where TModel : new()
    {

        public AppContract<TModel> Parameters { get; set; }
        public TModel Model { get; set; }
        public IEnumerable<ValidationResult> Errors { get; set; } = new List<ValidationResult>();
        public bool IsOk() => !Errors.Any();
        //public CurrentUser user;
    }

    public class AppContext<TParams> : AppContext<TParams, Object>
    {
    }
}
