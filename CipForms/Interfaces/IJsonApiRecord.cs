﻿using System;
namespace CipForms.Api.Interfaces
{
    public interface IJsonApiRecord
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        // TODO fix me later
        // public object attibutes { get; set; }
        public U ToPayload<U>() where U : new();
    }
}
