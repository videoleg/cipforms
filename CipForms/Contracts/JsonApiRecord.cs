﻿using System;
using AutoMapper;
using CipForms.Api.Interfaces;
using System.Collections.Generic;

namespace CipForms.Api.Contracts
{
    public class JsonApiRecord<T> : IJsonApiRecord
    {

        public Guid Id { get; set; }
        public string Type { get; set; }
        public T Attributes { get; set; }

        //// TODO public object relationships { get; set; }

        public U ToPayload<U>() where U : new()
        {
            var mapping = new MapperConfiguration(cfg =>
            {
                // TODO map relationships
                cfg.CreateMap<JsonApiRecord<T>, U>()
                    .AfterMap((src, dest, context) => context.Mapper.Map(src.Attributes, dest));
                cfg.CreateMap<T, U>();
            });

            var mapper = new Mapper(mapping);
            U payload = mapper.Map<U>(this);

            return payload;
        }

        // the property is needed to differintiate null values from not existed in a request
        private List<string> Changes = new() { "name" };
    }
}
