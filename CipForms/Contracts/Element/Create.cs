﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CipForms.Api.Contracts.Element
{
    public class Create
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
