﻿using System;
namespace CipForms.Api.Contracts
{
    public class JsonApiRequest<T> where T : new()
    {
        public T data;
    }
}
