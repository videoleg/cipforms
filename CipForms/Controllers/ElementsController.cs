﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CipForms.Data;
using CipForms.Logic.Models;
using Model = CipForms.Data.Models.Element;
using ApiContracts = CipForms.Api.Contracts;
using AppContracts = CipForms.Logic.Contracts;
using Operations = CipForms.Logic.Operations;

namespace CipForms.Api.Controllers
{
    //[ApiController]
    [Route("api/[controller]")]
    public class ElementsController : ControllerBase
    {
        private readonly ILogger<ElementsController> _logger;
        private readonly DatabaseContext _databaseContext;

        public ElementsController(
            DatabaseContext databaseContext,
            ILogger<ElementsController> logger
        )
        {
            _databaseContext = databaseContext;
            _logger = logger;
        }


        [HttpGet]
        public async Task<ActionResult<List<Model>>> Index()
        {
            return await new Operations.Element.Index(_databaseContext).InvokeAsync();
        }

        [HttpPost]
        public async Task<ActionResult<Model>> Create([FromBody] ApiContracts.JsonApiRecord<ApiContracts.Element.Create> dto)
        {
            var parameters = dto.ToPayload<AppContracts.Element.Create>();

            var context = new AppContext<AppContracts.Element.Create, Model>
            {
                Parameters = parameters
            };

            return await RunAsync<
                Operations.Element.Create,
                AppContracts.Element.Create
            >(context);
        }

        [HttpPut("{id:Guid}") ]
        public async Task<ActionResult<Model>> Update(
            [FromBody] ApiContracts.JsonApiRecord<ApiContracts.Element.Update> dto,
            [FromRoute] Guid id
        )
        {
            var parameters = dto.ToPayload<AppContracts.Element.Update>();
            var model = await _databaseContext.elements.FindAsync(id);
            var context = new AppContext<AppContracts.Element.Update, Model>
            {
                Parameters = parameters,
                Model = model
            };

            return await RunAsync<
                Operations.Element.Update,
                AppContracts.Element.Update
            >(context);
        }


        // abc.clario.com/element/1

        private async Task<ActionResult<Model>> RunAsync<TOperation, TParams>(AppContext<TParams, Model> context)
            where TOperation : Operations.AppOperation<TParams, Model>
            where TParams : AppContracts.AppContract<Model>

        {
            var operation = (TOperation)Activator.CreateInstance(typeof(TOperation), _databaseContext);
            var result = await operation.InvokeAsync(context);

            return ProcessResult(result);
        }

        private ActionResult<Model> ProcessResult<TParams>(AppContext<TParams, Model> result)
        {

            if (result.IsOk())
            {
                return result.Model;
            }
            else
            {
                return new BadRequestResult();
            }
        }
            
    }
}


