﻿using System;
using Microsoft.EntityFrameworkCore;
using CipForms.Data.Models;

namespace CipForms.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions ctx) : base(ctx) 
        {
        }

        public DbSet<Element> elements { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // applying model configuration
            modelBuilder.ApplyConfigurationsFromAssembly(
                assembly: typeof(DatabaseContext).Assembly
            );

            // extensions
            modelBuilder.HasPostgresExtension("citext");

            base.OnModelCreating(modelBuilder);
        }
    }
}
