﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CipForms.Data;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace CipForms.Data
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection RegisterDataServices(
            this IServiceCollection services,
            IConfiguration configuration
        )
        {
            // Database configuration
            var databaseConfig = new DatabaseConfiguration();
            configuration.GetSection("Database").Bind(databaseConfig);
            services.AddDbContext<DatabaseContext>(
                options => options.UseNpgsql(databaseConfig.ConnectionString)
            );

            return services;
        }
    }
}
