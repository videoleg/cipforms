﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CipForms.Data.Models;

namespace CipForms.Data.Configuration
{
    public class ElementConfiguration : IEntityTypeConfiguration<Element>
    {
        public void Configure (EntityTypeBuilder<Element> builder)
        {
            builder.ToTable("Element");
            builder.HasKey(r => r.id);
            builder.Property(r => r.Name).HasColumnType("citext");
            builder.Property(r => r.Description).HasColumnType("citext");
            builder.Property(r => r.CreatedAt).HasColumnType("timestamptz");
            builder.Property(r => r.UpdatedAt).HasColumnType("timestamptz");
        }
    }
}
