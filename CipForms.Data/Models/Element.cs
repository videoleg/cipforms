﻿using System;

namespace CipForms.Data.Models
{
    public class Element
    {
        public Guid? id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
