using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using Xunit;

namespace CipForms.Tests
{
    public class SerializerTests
    {
        [Fact]
        public void Should_Deserialize_Json()
        {
            var graph = new ResourceGraphBuilder()
                .Add<FormSchema, string>(publicName: "formSchema")
                .Add<FormNode, string>(publicName: "formNode")
                .Add<Element, string>(publicName: "element")
                .Build();


            var document = ParseDocument(graph);
        }

        private static Document ParseDocument(IResourceGraph graph)
        {
            return JsonSerializer.Deserialize<Document>(@"
{
  ""data"": {
    ""id"": ""123"",
    ""type"": ""formSchema"",
    ""attributes"": {
      ""version"": ""0.0.1""
    },
    ""relationships"": {
      ""formNodes"": {
        ""data"": [
          {
            ""id"": null, 
            ""type"": ""formNode"",
            ""attributes"": {
              ""kind"": ""page""
            },
            ""relationships"": {
              ""formNodes"": {
                ""data"": [
                  {
                    ""id"": null, 
                    ""type"": ""formNode"",
                    ""attributes"": {
                      ""kind"": ""input""
                    },
                    ""relationships"": {
                      ""element"": {
                        ""data"": {
                          ""id"": null,  
                          ""type"": ""element"",
                          ""attributes"": {
                            ""kind"": ""text"",
                            ""name"": ""super name""
                          }
                        }
                      }
                    }
                  }
                ]
              }
            }
          },
          {
            ""id"": ""123"", 
            ""type"": ""formNode"",
            ""attributes"": {
              ""kind"": ""page""
            },
            ""relationships"": {
              ""formNodes"": {
                ""data"": [
                  {
                    ""id"": ""555"", 
                    ""type"": ""formNode"",
                    ""attributes"": {
                      ""kind"": ""input""
                    },
                    ""relationships"": {
                      ""element"": {
                        ""data"": {
                          ""id"": ""888"",  
                          ""type"": ""element"",
                          ""attributes"": {
                            ""kind"": ""text"",
                            ""name"": ""new name""
                          }
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        ]
      }
    }
  }
}", new JsonSerializerOptions()
            {
                // These are the options common to serialization and deserialization.
                // At runtime, we actually use SerializerReadOptions and SerializerWriteOptions, which are customized copies of these settings,
                // to overcome the limitation in System.Text.Json that the JsonPath is incorrect when using custom converters.
                // Therefore we try to avoid using custom converters has much as possible.
                // https://github.com/Tarmil/FSharp.SystemTextJson/issues/37
                // https://github.com/dotnet/runtime/issues/50205#issuecomment-808401245

                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
                Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
                Converters =
                {
                    new SingleOrManyDataConverterFactory(),
                    new ResourceObjectConverter(graph),
                    new JsonStringEnumConverter(JsonNamingPolicy.CamelCase)
                }
            });
        }


        public class Document
        {
            [JsonPropertyName("data")]
            // JsonIgnoreCondition is determined at runtime by WriteOnlyDocumentConverter.
            public SingleOrManyData<ResourceObject> Data { get; set; }
        }

        public class SingleOrManyData<T>
            where T : class, IResourceIdentity, new()
        {
            // ReSharper disable once MergeConditionalExpression
            // Justification: ReSharper reporting this is a bug, which is fixed in v2021.2.1. This condition cannot be merged.
            public object? Value => ManyValue != null ? ManyValue : SingleValue;

            [JsonIgnore] public bool IsAssigned { get; }

            [JsonIgnore] public T? SingleValue { get; }

            [JsonIgnore] public IList<T>? ManyValue { get; }

            public SingleOrManyData(object? value)
            {
                IsAssigned = true;

                if (value is IEnumerable<T> manyData)
                {
                    ManyValue = manyData.ToList();
                    SingleValue = null;
                }
                else
                {
                    ManyValue = null;
                    SingleValue = (T?)value;
                }
            }
        }

        public interface IResourceIdentity
        {
            public string? Type { get; }

            public string? Id { get; }
        }

        public class ResourceObject : IResourceIdentity
        {
            [JsonPropertyName("type")]
            [JsonIgnore(Condition = JsonIgnoreCondition.Never)]
            public string? Type { get; set; }

            [JsonPropertyName("id")]
            [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            public string? Id { get; set; }
            
            [JsonPropertyName("attributes")]
            [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            public IDictionary<string, object?>? Attributes { get; set; }

            [JsonPropertyName("relationships")]
            [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            public IDictionary<string, RelationshipObject?>? Relationships { get; set; }
        }

        public class FormSchema : Identifiable<string>
        {
            [Attr(PublicName = "version")] public string Version { get; set; }

            [HasMany(PublicName = "formNodes")] public ICollection<FormNode> FormNodes { get; set; }
        }

        public class FormNode : Identifiable<string>
        {
            [Attr(PublicName = "kind")] public FormKind Kind { get; set; }

            [HasOne(PublicName = "element")] public Element Element { get; set; }

            [HasMany(PublicName = "formNodes")] public ICollection<FormNode> FromNodes { get; set; }
        }

        public enum FormKind
        {
            Unspecified = 0,
            Page,
            Input
        }

        public class Identifiable<TId> : IIdentifiable<TId>
        {
            public TId Id { get; set; }
        }

        public interface IIdentifiable<TId>
        {
            public TId Id { get; set; }
        }

        public class Element : Identifiable<string>
        {
            [Attr(PublicName = "kind")] public string Kind { get; set; }

            [Attr(PublicName = "name")] public string Name { get; set; }
        }

        [AttributeUsage(AttributeTargets.Property)]
        public sealed class AttrAttribute : ResourceFieldAttribute
        {
        }

        [AttributeUsage(AttributeTargets.Property)]
        public sealed class HasManyAttribute : RelationshipAttribute
        {
        }

        [AttributeUsage(AttributeTargets.Property)]
        public sealed class HasOneAttribute : RelationshipAttribute
        {
        }

        public abstract class RelationshipAttribute : ResourceFieldAttribute
        {
            public Type LeftClrType { get; set; }
            public Type RightClrType { get; set; }
            public ResourceType LeftType { get; set; }
            public ResourceType RightType { get; set; }
        }

        public abstract class ResourceFieldAttribute : Attribute
        {
            public string PublicName { get; set; }

            public PropertyInfo Property { get; set; }
        }

        public class ResourceGraphBuilder
        {
            private readonly Dictionary<Type, ResourceType> _resourceTypesByClrType = new();

            public ResourceGraphBuilder Add<TResource, TId>(string publicName)
                where TResource : class, IIdentifiable<TId>
            {
                return Add(typeof(TResource), typeof(TId), publicName);
            }

            public IResourceGraph Build()
            {
                HashSet<ResourceType> resourceTypes = _resourceTypesByClrType.Values.ToHashSet();

                if (!resourceTypes.Any())
                {
                    Console.WriteLine("The resource graph is empty.");
                    //_logger.LogWarning("The resource graph is empty.");
                }

                var resourceGraph = new ResourceGraph(resourceTypes);

                foreach (RelationshipAttribute relationship in resourceTypes.SelectMany(resourceType =>
                    resourceType.Relationships))
                {
                    relationship.LeftType = resourceGraph.GetResourceType(relationship.LeftClrType!);
                    ResourceType? rightType = resourceGraph.FindResourceType(relationship.RightClrType!);

                    if (rightType == null)
                    {
                        throw new InvalidOperationException($"Resource type '{relationship.LeftClrType}' depends on " +
                                                            $"'{relationship.RightClrType}', which was not added to the resource graph.");
                    }

                    relationship.RightType = rightType;
                }

                return resourceGraph;
            }

            public ResourceGraphBuilder Add(Type resourceClrType, Type idClrType, string publicName)
            {
                if (_resourceTypesByClrType.ContainsKey(resourceClrType))
                {
                    return this;
                }

                if (resourceClrType.IsOrImplementsInterface(typeof(IIdentifiable<string>)))
                {
                    string effectivePublicName = publicName;
                    Type? effectiveIdType = idClrType;

                    if (effectiveIdType == null)
                    {
                        throw new InvalidOperationException(
                            $"Resource type '{resourceClrType}' implements 'IIdentifiable', but not 'IIdentifiable<TId>'.");
                    }

                    ResourceType resourceType =
                        CreateResourceType(effectivePublicName, resourceClrType, effectiveIdType);

                    //AssertNoDuplicatePublicName(resourceType, effectivePublicName);

                    _resourceTypesByClrType.Add(resourceClrType, resourceType);
                }
                else
                {
                    Console.WriteLine(
                        $"Skipping: Type '{resourceClrType}' does not implement '{nameof(IIdentifiable<string>)}'.");
                    //_logger.LogWarning($"Skipping: Type '{resourceClrType}' does not implement '{nameof(IIdentifiable)}'.");
                }

                return this;
            }

            private ResourceType CreateResourceType(string publicName, Type resourceClrType, Type idClrType)
            {
                IReadOnlyCollection<AttrAttribute> attributes = GetAttributes(resourceClrType);
                IReadOnlyCollection<RelationshipAttribute> relationships = GetRelationships(resourceClrType);

                //AssertNoDuplicatePublicName(attributes, relationships);

                return new ResourceType(publicName, resourceClrType, idClrType, attributes, relationships);
            }

            private IReadOnlyCollection<RelationshipAttribute> GetRelationships(Type resourceClrType)
            {
                var relationshipsByName = new Dictionary<string, RelationshipAttribute>();
                PropertyInfo[] properties = resourceClrType.GetProperties();

                foreach (PropertyInfo property in properties)
                {
                    var relationship =
                        (RelationshipAttribute?)property.GetCustomAttribute(typeof(RelationshipAttribute));

                    if (relationship != null)
                    {
                        relationship.Property = property;
                        SetPublicName(relationship, property);
                        relationship.LeftClrType = resourceClrType;
                        relationship.RightClrType = GetRelationshipType(relationship, property);

                        IncludeField(relationshipsByName, relationship);
                    }
                }

                return relationshipsByName.Values;
            }

            private Type GetRelationshipType(RelationshipAttribute relationship, PropertyInfo property)
            {
                return relationship is HasOneAttribute
                    ? property.PropertyType
                    : property.PropertyType.GetGenericArguments()[0];
            }

            private IReadOnlyCollection<AttrAttribute> GetAttributes(Type resourceClrType)
            {
                var attributesByName = new Dictionary<string, AttrAttribute>();

                foreach (PropertyInfo property in resourceClrType.GetProperties())
                {
                    // Although strictly not correct, 'id' is added to the list of attributes for convenience.
                    // For example, it enables to filter on ID, without the need to special-case existing logic.
                    // And when using sparse fields, it silently adds 'id' to the set of attributes to retrieve.
                    if (property.Name == nameof(Identifiable<object>.Id))
                    {
                        var idAttr = new AttrAttribute
                        {
                            PublicName = property.Name,
                            Property = property,
                        };

                        IncludeField(attributesByName, idAttr);
                        continue;
                    }

                    var attribute = (AttrAttribute?)property.GetCustomAttribute(typeof(AttrAttribute));

                    if (attribute == null)
                    {
                        continue;
                    }

                    SetPublicName(attribute, property);
                    attribute.Property = property;

                    IncludeField(attributesByName, attribute);
                }

                if (attributesByName.Count < 2)
                {
                    Console.WriteLine($"Type '{resourceClrType}' does not contain any attributes.");
                    //_logger.LogWarning($"Type '{resourceClrType}' does not contain any attributes.");
                }

                return attributesByName.Values;
            }

            private void SetPublicName(ResourceFieldAttribute field, PropertyInfo property)
            {
                field.PublicName ??= property.Name;
            }

            private static void IncludeField<TField>(Dictionary<string, TField> fieldsByName, TField field)
                where TField : ResourceFieldAttribute
            {
                if (fieldsByName.TryGetValue(field.PublicName, out var existingField))
                {
                    throw new InvalidOperationException(
                        $"Properties '{field.Property.DeclaringType!}.{existingField.Property.Name}' and '{field.Property.DeclaringType!}.{field.Property.Name}' both use public name '{field.PublicName}'.");
                }

                fieldsByName.Add(field.PublicName, field);
            }
        }

        public class ResourceGraph : IResourceGraph
        {
            private readonly IReadOnlySet<ResourceType> _resourceTypeSet;
            private readonly Dictionary<Type, ResourceType> _resourceTypesByClrType = new();
            private readonly Dictionary<string, ResourceType> _resourceTypesByPublicName = new();

            public ResourceGraph(IReadOnlySet<ResourceType> resourceTypeSet)
            {
                _resourceTypeSet = resourceTypeSet;

                foreach (ResourceType resourceType in resourceTypeSet)
                {
                    _resourceTypesByClrType.Add(resourceType.ClrType, resourceType);
                    _resourceTypesByPublicName.Add(resourceType.PublicName, resourceType);
                }
            }

            public ResourceType? FindResourceType(Type resourceClrType)
            {
                return _resourceTypesByClrType.TryGetValue(resourceClrType, out ResourceType? resourceType)
                    ? resourceType
                    : null;
            }

            public ResourceType? FindResourceType(string publicName)
            {
                return _resourceTypesByPublicName.TryGetValue(publicName, out ResourceType? resourceType) ? resourceType : null;
            }

            public ResourceType GetResourceType(Type resourceClrType)
            {
                ResourceType? resourceType = FindResourceType(resourceClrType);

                if (resourceType == null)
                {
                    throw new InvalidOperationException($"Resource of type '{resourceClrType.Name}' does not exist.");
                }

                return resourceType;
            }
        }

        public interface IResourceGraph
        {
            ResourceType? FindResourceType(Type resourceClrType);
            
            ResourceType? FindResourceType(string publicName);

            ResourceType GetResourceType(Type resourceClrType);
        }

        /// <summary>
        /// Metadata about the shape of a JSON:API resource in the resource graph.
        /// </summary>
        public sealed class ResourceType
        {
            private readonly Dictionary<string, ResourceFieldAttribute> _fieldsByPublicName = new();
            private readonly Dictionary<string, ResourceFieldAttribute> _fieldsByPropertyName = new();

            /// <summary>
            /// The publicly exposed resource name.
            /// </summary>
            public string PublicName { get; }

            /// <summary>
            /// The CLR type of the resource.
            /// </summary>
            public Type ClrType { get; }

            /// <summary>
            /// The CLR type of the resource identity.
            /// </summary>
            public Type IdentityClrType { get; }

            /// <summary>
            /// Exposed resource attributes and relationships. See https://jsonapi.org/format/#document-resource-object-fields.
            /// </summary>
            public IReadOnlyCollection<ResourceFieldAttribute> Fields { get; }

            /// <summary>
            /// Exposed resource attributes. See https://jsonapi.org/format/#document-resource-object-attributes.
            /// </summary>
            public IReadOnlyCollection<AttrAttribute> Attributes { get; }

            /// <summary>
            /// Exposed resource relationships. See https://jsonapi.org/format/#document-resource-object-relationships.
            /// </summary>
            public IReadOnlyCollection<RelationshipAttribute> Relationships { get; }

            public ResourceType(string publicName, Type clrType, Type identityClrType,
                IReadOnlyCollection<AttrAttribute>? attributes = null,
                IReadOnlyCollection<RelationshipAttribute>? relationships = null)
            {
                PublicName = publicName;
                ClrType = clrType;
                IdentityClrType = identityClrType;
                Attributes = attributes ?? Array.Empty<AttrAttribute>();
                Relationships = relationships ?? Array.Empty<RelationshipAttribute>();
                Fields = Attributes.Cast<ResourceFieldAttribute>().Concat(Relationships).ToArray();
                
                foreach (ResourceFieldAttribute field in Fields)
                {
                    _fieldsByPublicName.Add(field.PublicName, field);
                    _fieldsByPropertyName.Add(field.Property.Name, field);
                }
            }

            public AttrAttribute GetAttributeByPublicName(string publicName)
            {
                AttrAttribute? attribute = FindAttributeByPublicName(publicName);
                return attribute ??
                       throw new InvalidOperationException(
                           $"Attribute '{publicName}' does not exist on resource type '{PublicName}'.");
            }

            public AttrAttribute? FindAttributeByPublicName(string publicName)
            {
                return _fieldsByPublicName.TryGetValue(publicName, out ResourceFieldAttribute? field) &&
                       field is AttrAttribute attribute
                    ? attribute
                    : null;
            }

            public AttrAttribute GetAttributeByPropertyName(string propertyName)
            {
                AttrAttribute? attribute = FindAttributeByPropertyName(propertyName);

                return attribute ??
                       throw new InvalidOperationException(
                           $"Attribute for property '{propertyName}' does not exist on resource type '{ClrType.Name}'.");
            }

            public AttrAttribute? FindAttributeByPropertyName(string propertyName)
            {
                return _fieldsByPropertyName.TryGetValue(propertyName, out ResourceFieldAttribute? field) &&
                       field is AttrAttribute attribute
                    ? attribute
                    : null;
            }

            public RelationshipAttribute GetRelationshipByPublicName(string publicName)
            {
                RelationshipAttribute? relationship = FindRelationshipByPublicName(publicName);
                return relationship ??
                       throw new InvalidOperationException(
                           $"Relationship '{publicName}' does not exist on resource type '{PublicName}'.");
            }

            public RelationshipAttribute? FindRelationshipByPublicName(string publicName)
            {
                return _fieldsByPublicName.TryGetValue(publicName, out ResourceFieldAttribute? field) &&
                       field is RelationshipAttribute relationship
                    ? relationship
                    : null;
            }

            public RelationshipAttribute GetRelationshipByPropertyName(string propertyName)
            {
                RelationshipAttribute? relationship = FindRelationshipByPropertyName(propertyName);

                return relationship ??
                       throw new InvalidOperationException(
                           $"Relationship for property '{propertyName}' does not exist on resource type '{ClrType.Name}'.");
            }

            public RelationshipAttribute? FindRelationshipByPropertyName(string propertyName)
            {
                return _fieldsByPropertyName.TryGetValue(propertyName, out ResourceFieldAttribute? field) &&
                       field is RelationshipAttribute relationship
                    ? relationship
                    : null;
            }

            public override string ToString()
            {
                return PublicName;
            }

            public override bool Equals(object? obj)
            {
                if (ReferenceEquals(this, obj))
                {
                    return true;
                }

                if (obj is null || GetType() != obj.GetType())
                {
                    return false;
                }

                var other = (ResourceType)obj;

                return PublicName == other.PublicName && ClrType == other.ClrType &&
                       IdentityClrType == other.IdentityClrType &&
                       Attributes.SequenceEqual(other.Attributes) && Relationships.SequenceEqual(other.Relationships);
            }

            public override int GetHashCode()
            {
                var hashCode = new HashCode();

                hashCode.Add(PublicName);
                hashCode.Add(ClrType);
                hashCode.Add(IdentityClrType);

                foreach (AttrAttribute attribute in Attributes)
                {
                    hashCode.Add(attribute);
                }

                foreach (RelationshipAttribute relationship in Relationships)
                {
                    hashCode.Add(relationship);
                }

                return hashCode.ToHashCode();
            }
        }
    }

    internal static class TypeExtensions
    {
        /// <summary>
        /// Whether the specified source type implements or equals the specified interface.
        /// </summary>
        public static bool IsOrImplementsInterface(this Type? source, Type interfaceType)
        {
            if (source == null)
            {
                return false;
            }

            return source == interfaceType || source.GetInterfaces().Any(type => type == interfaceType);
        }
    }

    public sealed class SingleOrManyDataConverterFactory : JsonConverterFactory
    {
        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert.IsGenericType &&
                   typeToConvert.GetGenericTypeDefinition() == typeof(SerializerTests.SingleOrManyData<>);
        }

        public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
        {
            Type objectType = typeToConvert.GetGenericArguments()[0];
            Type converterType = typeof(SingleOrManyDataConverter<>).MakeGenericType(objectType);

            return (JsonConverter)Activator.CreateInstance(converterType, BindingFlags.Instance | BindingFlags.Public,
                null, null, null)!;
        }

        private sealed class SingleOrManyDataConverter<T> : JsonObjectConverter<SerializerTests.SingleOrManyData<T>>
            where T : class, SerializerTests.IResourceIdentity, new()
        {
            public override SerializerTests.SingleOrManyData<T> Read(ref Utf8JsonReader reader, Type typeToConvert,
                JsonSerializerOptions serializerOptions)
            {
                var objects = new List<T?>();
                bool isManyData = false;
                bool hasCompletedToMany = false;

                do
                {
                    switch (reader.TokenType)
                    {
                        case JsonTokenType.EndArray:
                        {
                            hasCompletedToMany = true;
                            break;
                        }
                        case JsonTokenType.Null:
                        {
                            if (isManyData)
                            {
                                objects.Add(new T());
                            }

                            break;
                        }
                        case JsonTokenType.StartObject:
                        {
                            var resourceObject = ReadSubTree<T>(ref reader, serializerOptions);
                            objects.Add(resourceObject);
                            break;
                        }
                        case JsonTokenType.StartArray:
                        {
                            isManyData = true;
                            break;
                        }
                    }
                } while (isManyData && !hasCompletedToMany && reader.Read());

                object? data = isManyData ? objects : objects.FirstOrDefault();
                return new SerializerTests.SingleOrManyData<T>(data);
            }

            public override void Write(Utf8JsonWriter writer, SerializerTests.SingleOrManyData<T> value,
                JsonSerializerOptions options)
            {
                WriteSubTree(writer, value.Value, options);
            }
        }
    }

    public abstract class JsonObjectConverter<TObject> : JsonConverter<TObject>
    {
        protected static TValue? ReadSubTree<TValue>(ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            if (typeof(TValue) != typeof(object) &&
                options.GetConverter(typeof(TValue)) is JsonConverter<TValue> converter)
            {
                return converter.Read(ref reader, typeof(TValue), options);
            }

            return JsonSerializer.Deserialize<TValue>(ref reader, options);
        }

        protected static void WriteSubTree<TValue>(Utf8JsonWriter writer, TValue value, JsonSerializerOptions options)
        {
            if (typeof(TValue) != typeof(object) &&
                options.GetConverter(typeof(TValue)) is JsonConverter<TValue> converter)
            {
                converter.Write(writer, value, options);
            }
            else
            {
                JsonSerializer.Serialize(writer, value, options);
            }
        }

        protected static JsonException GetEndOfStreamError()
        {
            return new JsonException("Unexpected end of JSON stream.");
        }
    }
    
    public sealed class ResourceObjectConverter : JsonObjectConverter<SerializerTests.ResourceObject>
    {
        private static readonly JsonEncodedText TypeText = JsonEncodedText.Encode("type");
        private static readonly JsonEncodedText IdText = JsonEncodedText.Encode("id");
        private static readonly JsonEncodedText AttributesText = JsonEncodedText.Encode("attributes");
        private static readonly JsonEncodedText RelationshipsText = JsonEncodedText.Encode("relationships");

        private readonly SerializerTests.IResourceGraph _resourceGraph;

        public ResourceObjectConverter(SerializerTests.IResourceGraph resourceGraph)
        {
            _resourceGraph = resourceGraph;
        }

        /// <summary>
        /// Resolves the resource type and attributes against the resource graph. Because attribute values in <see cref="SerializerTests.ResourceObject" /> are typed as
        /// <see cref="object" />, we must lookup and supply the target type to the serializer.
        /// </summary>
        public override SerializerTests.ResourceObject Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            // Inside a JsonConverter there is no way to know where in the JSON object tree we are. And the serializer is unable to provide
            // the correct position either. So we avoid an exception on missing/invalid 'type' element and postpone producing an error response
            // to the post-processing phase.

            var resourceObject = new SerializerTests.ResourceObject
            {
                // The 'attributes' element may occur before 'type', but we need to know the resource type before we can deserialize attributes
                // into their corresponding CLR types.
                Type = PeekType(ref reader)
            };

            SerializerTests.ResourceType? resourceType = resourceObject.Type != null ? _resourceGraph.FindResourceType(resourceObject.Type) : null;

            while (reader.Read())
            {
                switch (reader.TokenType)
                {
                    case JsonTokenType.EndObject:
                    {
                        return resourceObject;
                    }
                    case JsonTokenType.PropertyName:
                    {
                        string? propertyName = reader.GetString();
                        reader.Read();

                        switch (propertyName)
                        {
                            case "id":
                            {
                                if (reader.TokenType != JsonTokenType.String && reader.TokenType != JsonTokenType.Null)
                                {
                                    // Newtonsoft.Json used to auto-convert number to strings, while System.Text.Json does not. This is so likely
                                    // to hit users during upgrade that we special-case for this and produce a helpful error message.
                                    var jsonElement = ReadSubTree<JsonElement>(ref reader, options);
                                    throw new JsonException($"Failed to convert ID '{jsonElement}' of type '{jsonElement.ValueKind}' to type 'String'.");
                                }

                                resourceObject.Id = reader.GetString();
                                break;
                            }
                            case "attributes":
                            {
                                if (resourceType != null)
                                {
                                    resourceObject.Attributes = ReadAttributes(ref reader, options, resourceType);
                                }
                                else
                                {
                                    reader.Skip();
                                }

                                break;
                            }
                            case "relationships":
                            {
                                resourceObject.Relationships = ReadSubTree<IDictionary<string, RelationshipObject?>>(ref reader, options);
                                break;
                            }
                            default:
                            {
                                reader.Skip();
                                break;
                            }
                        }

                        break;
                    }
                }
            }

            throw GetEndOfStreamError();
        }

        private static string? PeekType(ref Utf8JsonReader reader)
        {
            // https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-converters-how-to?pivots=dotnet-5-0#an-alternative-way-to-do-polymorphic-deserialization
            Utf8JsonReader readerClone = reader;

            while (readerClone.Read())
            {
                if (readerClone.TokenType == JsonTokenType.PropertyName)
                {
                    string? propertyName = readerClone.GetString();
                    readerClone.Read();

                    switch (propertyName)
                    {
                        case "type":
                        {
                            return readerClone.GetString();
                        }
                        default:
                        {
                            readerClone.Skip();
                            break;
                        }
                    }
                }
            }

            return null;
        }

        private static IDictionary<string, object?> ReadAttributes(ref Utf8JsonReader reader, JsonSerializerOptions options, SerializerTests.ResourceType resourceType)
        {
            var attributes = new Dictionary<string, object?>();

            while (reader.Read())
            {
                switch (reader.TokenType)
                {
                    case JsonTokenType.EndObject:
                    {
                        return attributes;
                    }
                    case JsonTokenType.PropertyName:
                    {
                        string attributeName = reader.GetString() ?? string.Empty;
                        reader.Read();

                        SerializerTests.AttrAttribute? attribute = resourceType.FindAttributeByPublicName(attributeName);
                        PropertyInfo? property = attribute?.Property;

                        if (property != null)
                        {
                            object? attributeValue;

                            if (property.Name == nameof(SerializerTests.Identifiable<object>.Id))
                            {
                                attributeValue = JsonInvalidAttributeInfo.Id;
                            }
                            else
                            {
                                try
                                {
                                    attributeValue = JsonSerializer.Deserialize(ref reader, property.PropertyType, options);
                                }
                                catch (JsonException ex)
                                {
                                    // Inside a JsonConverter there is no way to know where in the JSON object tree we are. And the serializer
                                    // is unable to provide the correct position either. So we avoid an exception and postpone producing an error
                                    // response to the post-processing phase, by setting a sentinel value.
                                    var jsonElement = ReadSubTree<JsonElement>(ref reader, options);

                                    attributeValue = new JsonInvalidAttributeInfo(attributeName, property.PropertyType, jsonElement.ToString(),
                                        jsonElement.ValueKind);
                                }
                            }

                            attributes.Add(attributeName, attributeValue);
                        }
                        else
                        {
                            attributes.Add(attributeName, null);
                            reader.Skip();
                        }

                        break;
                    }
                }
            }

            throw GetEndOfStreamError();
        }

        /// <summary>
        /// Ensures that attribute values are not wrapped in <see cref="JsonElement" />s.
        /// </summary>
        public override void Write(Utf8JsonWriter writer, SerializerTests.ResourceObject value, JsonSerializerOptions options)
        {
            writer.WriteStartObject();

            writer.WriteString(TypeText, value.Type);

            if (value.Id != null)
            {
                writer.WriteString(IdText, value.Id);
            }

            if (!value.Attributes.IsNullOrEmpty())
            {
                writer.WritePropertyName(AttributesText);
                WriteSubTree(writer, value.Attributes, options);
            }

            if (!value.Relationships.IsNullOrEmpty())
            {
                writer.WritePropertyName(RelationshipsText);
                WriteSubTree(writer, value.Relationships, options);
            }

            writer.WriteEndObject();
        }
    }

    internal static class CollectionExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T>? source)
        {
            if (source == null)
            {
                return true;
            }

            return !source.Any();
        }
    }

    /// <summary>
    /// A sentinel value that is temporarily stored in the attributes dictionary to postpone producing an error.
    /// </summary>
    internal sealed class JsonInvalidAttributeInfo
    {
        public static readonly JsonInvalidAttributeInfo Id = new("id", typeof(string), "-", JsonValueKind.Undefined);

        public string AttributeName { get; }
        public Type AttributeType { get; }
        public string? JsonValue { get; }
        public JsonValueKind JsonType { get; }

        public JsonInvalidAttributeInfo(string attributeName, Type attributeType, string? jsonValue, JsonValueKind jsonType)
        {
            AttributeName = attributeName;
            AttributeType = attributeType;
            JsonValue = jsonValue;
            JsonType = jsonType;
        }
    }
    
    public sealed class RelationshipObject
    {
        [JsonPropertyName("data")]
        // JsonIgnoreCondition is determined at runtime by WriteOnlyRelationshipObjectConverter.
        public SerializerTests.SingleOrManyData<SerializerTests.ResourceObject> Data { get; set; }
    }
}